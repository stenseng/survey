#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Mar  2 13:33:42 2020

@author: Lars Stenseng
"""

import unittest
from survey.observation.observation import Observations, DoubletError
from survey.observation.observation import HeightDiff, Vector2D, Vector3D
from survey.observation.observation import TotalObs, Local2D, Local3D


class TestObservationClass(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls._obs = Observations()

    def test_HeightDiff(self):
        niv1 = HeightDiff(1, 's1', 'p1', 10.123, 1510.1, 2019.11, 0.1)
        self._obs.addObservation(niv1)
        niv2 = HeightDiff(2, 's1', 'p1', 10.123, 1510.1)
        self._obs.addObservation(niv2)

    def test_Vector3D(self):
        v3d1 = Vector3D(3, 's1', 'p1', 1510.5, 1.024, 10.234, 2019.91,
                        0.001, 0.010, 0.005, 0.001, 0.010, 0.004)
        self._obs.addObservation(v3d1)
        v3d2 = Vector3D(4, 's1', 'p1', 1510.5, 1.024, 10.234, 2019.91,
                        0.001, 0.010, 0.005)
        self._obs.addObservation(v3d2)
        v3d3 = Vector3D(5, 's1', 'p1', 1510.5, 1.024, 10.234)
        self._obs.addObservation(v3d3)

    def test_Vector2D(self):
        v2d1 = Vector2D(6, 's1', 'p1', 1510.5, 1.024, 2019.91,
                        0.001, 0.010, 0.004)
        self._obs.addObservation(v2d1)
        v2d2 = Vector2D(7, 's1', 'p1', 1510.5, 1.024, 2019.91,
                        0.001, 0.010, 0.005)
        self._obs.addObservation(v2d2)
        v2d3 = Vector2D(8, 's1', 'p1', 1510.5, 1.024)
        self._obs.addObservation(v2d3)

    def test_TotalObs(self):
        tot1 = TotalObs(9, 's1', 'p1', 390.123, 101.256, 50.234, 1, 2019.98,
                        0.002, 0.002, 0.002, 0.003, 0.012, 0.007)
        self._obs.addObservation(tot1)
        tot2 = TotalObs(10, 's1', 'p1', 350.000, 101.000, 150.00, 2, 2019.98,
                        0.0015, 0.0015, 0.002)
        self._obs.addObservation(tot2)
        tot3 = TotalObs(11, 's1', 'p1', 350.000, 101.000, 150.00)
        self._obs.addObservation(tot3)
        print(tot2, tot2.returnVector3D())

    def test_Local2D(self):
        l2d1 = Local2D(12, 'sys1', 'p1', -15.987, 12.467, 2019.98,
                       0.005, 0.005, 0.002)
        self._obs.addObservation(l2d1)
        l2d2 = Local2D(13, 'sys1', 'p1', -15.987, 12.467, 2019.98,
                       0.005, 0.005)
        self._obs.addObservation(l2d2)
        l2d3 = Local2D(14, 'sys1', 'p1', -15.987, 12.467)
        self._obs.addObservation(l2d3)

    def test_Local3D(self):
        l3d1 = Local3D(15, 'sys2', 'p1', 15.987, -12.467, 1.001, 2019.98,
                       0.005, 0.005, 0.001, 0.0025, 0.003, 0.005)
        self._obs.addObservation(l3d1)
        l3d2 = Local3D(16, 'sys2', 'p1', 15.987, -12.467, 1.001, 2019.98,
                       0.005, 0.005, 0.001)
        self._obs.addObservation(l3d2)
        l3d3 = Local3D(17, 'sys2', 'p1', 15.987, -12.467, 1.001)
        self._obs.addObservation(l3d3)

    def test_add_observation_exceptinon(self):
        niv = HeightDiff(100, 's1', 'p1', 10.123, 1510.1, 2019.11, 0.1)
        self._obs.addObservation(niv)
        with self.assertRaises(DoubletError):
            self._obs.addObservation(niv)

    def test_HeightDiff_compare(self):
        niv1 = HeightDiff(1, 's1', 'p1', 10.123, 1510.1)
        niv2 = niv1
        niv3 = HeightDiff(3, 's1', 'p3', 0.0, 1510.1)
        self.assertTrue(niv1 == niv2)
        self.assertFalse(niv1 == niv3)
        self.assertTrue(niv1 != niv3)
        self.assertFalse(niv1 != niv2)

    def test_HeightDiff_add(self):
        niv1 = HeightDiff(1, 's1', 'p1', 10.123, 1510.1)
        niv2 = HeightDiff(2, 's1', 'p1', 10.246, 1510.1)
        self.assertEqual(niv1 + 0.123, niv2)
        self.assertEqual(0.123 + niv1, niv2)
        niv3 = niv1
        niv3 += 0.123
        self.assertEqual(niv3, niv2)

    def test_HeightDiff_sub(self):
        niv1 = HeightDiff(1, 's1', 'p1', 10.123, 1510.1)
        niv2 = HeightDiff(2, 's1', 'p1', 10.000, 1510.1)
        self.assertEqual(niv1 - 0.123, niv2)
        self.assertEqual(20.123 - niv1, niv2)
        niv3 = niv1
        niv3 -= 0.123
        self.assertEqual(niv3, niv2)

    def test_HeightDiff_mul(self):
        niv1 = HeightDiff(1, 's1', 'p1', 10.000, 1510.1)
        niv2 = HeightDiff(1, 's1', 'p1', 20.000, 1510.1)
        niv3 = HeightDiff(1, 's1', 'p1', 10.000, 1510.1)
        self.assertEqual(niv1 * 2, niv2)
        self.assertEqual(2.0 * niv1, niv2)
        niv3 *= 2
        self.assertEqual(niv3, niv2)

    def test_HeightDiff_div(self):
        niv1 = HeightDiff(1, 's1', 'p1', 10.000, 1510.1)
        niv2 = HeightDiff(1, 's1', 'p1', 5.000, 1510.1)
        niv3 = HeightDiff(1, 's1', 'p1', 10.000, 1510.1)
        self.assertEqual(niv1 / 2.0, niv2)
        niv3 /= 2
        self.assertEqual(niv3, niv2)

    def test_returnCartesian(self):
        self._obs.returnCartesian()

    @classmethod
    def tearDownClass(cls):
        print("\nObservation DataFrame:\n", cls._obs.observations)


if __name__ == "__main__":
    unittest.main()
