#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Mar  2 13:33:42 2020

@author: Lars Stenseng
"""

import unittest
from survey.observation.point import Points, DoubletError, DimensionError
from survey.observation.point import Height, Position, Position3D


class TestPointsClass(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls._pos = Points()

    def test_add_height(self):
        h1 = Height('a', 10.000, 2019.9, 0.001)
        self._pos.addPoint(h1)
        self.assertEqual(self._pos.getHeight('a'), 10.0)
        with self.assertRaises(DoubletError):
            self._pos.addPoint(h1)

    def test_add_position(self):
        p1 = Position('b', 200.000, 500.000, 2019.9, 0.010, 0.025, 0.015)
        self._pos.addPoint(p1)
        with self.assertRaises(DoubletError):
            self._pos.addPoint(p1)

    def test_add_position3D(self):
        p3 = Position3D('c', 300.000, 600.000, 30.000, 2019.9,
                        0.015, 0.03, 0.02, 0.02, 0.015, 0.005)
        self._pos.addPoint(p3)
        with self.assertRaises(DoubletError):
            self._pos.addPoint(p3)

    def test_getHeight(self):
        self.assertEqual(self._pos.getHeight('a'), 10.0)
        with self.assertRaises(DimensionError):
            self._pos.getHeight('b')
        self.assertEqual(self._pos.getHeight('c'), 30.0)

    def test_getPosition(self):
        with self.assertRaises(DimensionError):
            self._pos.getPosition('a')
        self.assertEqual(self._pos.getPosition('b')[0], 200.000)
        self.assertEqual(self._pos.getPosition('b')[1], 500.000)
        self.assertEqual(self._pos.getPosition('c')[0], 300.000)
        self.assertEqual(self._pos.getPosition('c')[1], 600.000)

    def test_getPosition3D(self):
        with self.assertRaises(DimensionError):
            self._pos.getPosition3D('a')
        with self.assertRaises(DimensionError):
            self._pos.getPosition3D('b')
        self.assertEqual(self._pos.getPosition3D('c')[0], 300.000)
        self.assertEqual(self._pos.getPosition3D('c')[1], 600.000)
        self.assertEqual(self._pos.getPosition3D('c')[2], 30.000)

    def test_replace_height(self):
        h1 = Height('a', 12.000, 2019.9, 0.002)
        self._pos.replacePoint(h1)
        # print("\nPoints DataFrame (a replaced):\n", self._pos.points)

    def test_merge_height(self):
        h1 = Height('b', 20.000, 2019.9, 0.002)
        self._pos.mergePoint(h1)
        h2 = Height('a', 13.000, 2019.9, 0.002)
        with self.assertRaises(DoubletError):
            self._pos.mergePoint(h2)

    def test_merge_pos(self):
        p1 = Position('a', 100.000, 400.000, 2019.9, 0.010, 0.025)
        self._pos.mergePoint(p1)
        p2 = Position('a', 110.000, 410.000, 2019.9, 0.010, 0.025)
        with self.assertRaises(DoubletError):
            self._pos.mergePoint(p2)

    def test_pivotPoints(self):
        print("\nPoint DataFrame (before pivot):\n", self._pos.points)
        self._pos.pivotPoints()
        print("Pivot: ", self._pos.pivotE, self._pos.pivotN, self._pos.pivotH)
        print("Point DataFrame (after pivot):\n", self._pos.points)
        self._pos.unpivotPoints()
        print("Pivot: ", self._pos.pivotE, self._pos.pivotN, self._pos.pivotH)
        print("Point DataFrame (restore pivot):\n", self._pos.points)

    @classmethod
    def tearDownClass(cls):
        pass


if __name__ == "__main__":
    unittest.main()
