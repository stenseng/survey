#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Oct 30 10:26:27 2019

@author: Lars Stenseng
"""

import pandas as pd
import numpy as np


class Error(Exception):
    """Base class for position exceptions"""
    pass


class DoubletError(Error):
    """Exceptions relatede to reuse of a pointId"""

    def __init__(self, message):
        self.message = message


class EpochError(Error):
    """Exceptions related to mismatch in epochs"""

    def __init__(self, message):
        self.message = message


class DimensionError(Error):
    """Exceptions related to mismatch in dimensions"""

    def __init__(self, message):
        self.message = message


class Height:
    """Class for points with a height

    The class provides methods for comparing the height of points and perform
    simple calculations like adding/subtracting an offset from the height.
    """

    def __init__(self, pointId: str, height: float, epoch: float = -0.0,
                 stdHeight: float = -0.0, epsg: int = -1) -> None:
        self.pointId = pointId
        self.dim = 1
        self.epsg = epsg
        self.epoch = epoch
        self.height = height
        self.stdHeight = stdHeight
        self.posTol = 0.0001

    def data(self) -> pd.DataFrame():
        data = pd.DataFrame([[self.pointId, self.epsg, self.epoch, self.dim,
                              self.height, self.stdHeight]],
                            columns=('pointId', 'epsg', 'epoch', 'dim',
                                     'height', 'stdH'))
        return data

    def __repr__(self):
        return (f'{self.__class__.__name__}({self.pointId}, {self.height}, '
                f'{self.epoch}, {self.stdHeight})')

    def __str__(self):
        return (f'({self.height:.4f} m \u00b1{self.stdHeight:.4f} m)\n')

    def __eq__(self, other):
        if (abs(self.height - other.height) < self.posTol
                and self.epsg == other.epsg):
            return True
        else:
            return False

    def __add__(self, other):
        other.height = self.height + other.height
        return other

    def __sub__(self, other):
        other.height = self.height - other.height
        return other


class Position:

    def __init__(self, pointId: str, e: float, n: float,
                 epoch: float = -0.0, stdE: float = -0.0, stdN: float = -0.0,
                 covEN: float = -0.0, epsg: int = -1):
        self.pointId = pointId
        self.dim = 2
        self.epsg = epsg
        self.epoch = epoch
        self.e = e
        self.n = n
        self.stdE = stdE
        self.stdN = stdN
        self.covEN = covEN
        self.posTol = 0.0001

    def data(self):
        data = pd.DataFrame([[self.pointId, self.epsg, self.epoch, self.dim,
                              self.e, self.n, self.stdE, self.stdN,
                              self.covEN]],
                            columns=('pointId', 'epsg', 'epoch', 'dim', 'e',
                                     'n', 'stdE', 'stdN', 'covEN'))
        return data

    def __eq__(self, other):
        if (abs(self.e - other.e) < self.posTol
                and abs(self.n - other.n) < self.posTol
                and self.epsg == other.epsg):
            return True
        else:
            return False

    def __add__(self, other):
        other.e = self.e + other.e
        other.n = self.n + other.n
        return other

    def __sub__(self, other):
        other.e = self.e - other.e
        other.n = self.n - other.n
        return other


class Position3D:

    def __init__(self, pointId, e, n, height,
                 epoch=-0.0, stdE=-0.0, stdN=-0.0, stdHeight=-0.0,
                 covEN=-0.0, covEH=-0.0, covNH=-0.0, epsg=-1):
        self.pointId = pointId
        self.dim = 3
        self.epsg = epsg
        self.epoch = epoch
        self.e = e
        self.n = n
        self.height = height
        self.stdE = stdE
        self.stdN = stdN
        self.stdHeight = stdHeight
        self.covEN = covEN
        self.covEH = covEH
        self.covNH = covNH
        self.posTol = 0.0001

    def data(self):
        return pd.DataFrame([[self.pointId, self.epsg, self.epoch, self.dim,
                              self.e, self.n, self.height,
                              self.stdE, self.stdN, self.stdHeight,
                              self.covEN, self.covEH, self.covNH]],
                            columns=('pointId', 'epsg', 'epoch', 'dim',
                                     'e', 'n', 'height',
                                     'stdE', 'stdN', 'stdH',
                                     'covEN', 'covEH', 'covNH'))

    def __eq__(self, other):
        if (abs(self.e - other.e) < self.posTol
                and abs(self.n - other.n) < self.posTol
                and abs(self.height - other.height) < self.posTol
                and self.epsg == other.epsg):
            return True
        else:
            return False

    def __add__(self, other):
        other.e = self.e + other.e
        other.n = self.n + other.n
        other.height = self.height + other.height
        return other

    def __sub__(self, other):
        other.e = self.e - other.e
        other.n = self.n - other.n
        other.height = self.height - other.height
        return other


class Points:
    """Container class for points with position and/or height used in surveying

    The class provides a dataframe to store point objects and methods to
    perform common tasks on the points.
    """

    def __init__(self):
        self.epochTol = 0.02    # Consider epochs witin a week to be equal
        self.posTol = 0.0001    # Consider positions within 1/10 mm to be equal
        self.pivot = False
        self.pivotE = 0.0
        self.pivotN = 0.0
        self.pivotH = 0.0
        self.points = pd.DataFrame(columns=['pointId', 'epsg', 'epoch',
                                            'dim', 'e', 'n', 'height',
                                            'stdE', 'stdN', 'stdH',
                                            'covEN', 'covEH', 'covNH'])

    def addPoint(self, point):
        if point.pointId not in self.points.pointId.to_numpy():
            self.points = self.points\
                                 .append(point.data(),
                                         ignore_index=True,
                                         sort=False)\
                                 .sort_values(by='pointId', ignore_index=True)
        else:
            raise DoubletError('\nWarning! Point already exists\n\n'
                               ' Use mergePosition() or '
                               'replacePosition() instead')

    def getPoint(self, pointId):
        # Cropping dataframe to only have data by pointId
        point = self.points[self.points.pointId == pointId].reset_index()
        if point['dim'][0] == 1:
            return Height(point.pointId[0], point.height[0], point.epoch[0],
                          point.stdH[0], point.epsg[0])
        elif point['dim'][0] == 2:
            return Position(point.pointId[0], point.e[0], point.n[0],
                            point.epoch[0], point.stdE[0], point.stdN[0],
                            point.covEN[0])
        else:
            return Position3D(point.pointId[0], point.e[0], point.n[0],
                              point.height[0], point.epoch[0], point.epsg[0],
                              point.stdE[0], point.stdN[0], point.stdH[0],
                              point.covEN[0], point.covEH[0], point.covNH[0])

    def getHeight(self, pointId):
        point = self.getPoint(pointId)
        if point.dim == 1 or point.dim == 3:
            return self.getPoint(pointId).height
        else:
            raise DimensionError('\nWarning! Point do not exist for\n\n'
                                 'getHeight(). Try getPosition() or '
                                 ' instead')

    def getPosition(self, pointId):
        point = self.getPoint(pointId)
        if point.dim == 2 or point.dim == 3:
            return np.array([self.getPoint(pointId).e,
                             self.getPoint(pointId).n])
        else:
            raise DimensionError('\nWarning! Point do not exist for\n\n'
                                 'getPosition(). Try getHeight() instead')

    def getPosition3D(self, pointId):
        point = self.getPoint(pointId)
        if point.dim == 3:
            return np.array([self.getPoint(pointId).e,
                             self.getPoint(pointId).n,
                             self.getPoint(pointId).height])
        else:
            raise DimensionError('\nWarning! Point do not exist for \n\n'
                                 'getPosition3D().')

    def mergePoint(self, point, ignore_epoch=False):
        if point.pointId in self.points.pointId.to_numpy():
            [[existEpoch, existDim]] = self.points.loc[
                self.points.pointId == point.pointId,
                ['epoch', 'dim']].to_numpy()
            if ((existEpoch - point.epoch) < self.epochTol
                    or ignore_epoch or existEpoch < 0
                    or point.epoch < 0):
                if (existDim + point.dim) == 3:
                    if point.dim == 1:
                        self.points.loc[self.points.pointId
                                        == point.pointId,
                                        ['height', 'stdH', 'dim']]\
                            = (point.height, point.stdHeight, 3)
                    elif point.dim == 2:
                        self.points.loc[self.points.pointId
                                        == point.pointId,
                                        ['e', 'n', 'stdE', 'stdN', 'dim']]\
                            = (point.e, point.n,
                               point.stdE, point.stdN, 3)
                else:
                    raise DoubletError('\nWarning! Merging points with '
                                       'wrong dimensions\n\n'
                                       ' Use replacePosition() instead')
            else:
                raise EpochError('\nWarning! '
                                 'Epochs in point merge mismatchs\n\n'
                                 ' Set ignore_epoch=True to override error')
        else:
            raise DoubletError('\nWarning! '
                               'Merging with a nonexisting point\n\n'
                               ' Use addPosition() instead')

    def replacePoint(self, point):
        if point.pointId in self.points.pointId.to_numpy():
            self.points\
                = self.points[self.points.pointId != point.pointId]\
                .append(point.data(), ignore_index=True, sort=False)\
                .sort_values(by='pointId', ignore_index=True)
        else:
            raise DoubletError('\nWarning! Replacing a nonexisting point\n\n'
                               ' Use addPosition() instead')

    def pivotPoints(self):
        if not self.pivot:
            [self.pivotE, self.pivotN, self.pivotH] =\
                self.points.mean()[['e', 'n', 'height']].to_numpy()
            self.points[['e', 'n', 'height']]\
                = self.points[['e', 'n', 'height']]\
                - [self.pivotE, self.pivotN, self.pivotH]
            self.pivot = True
        else:
            pass

    def unpivotPoints(self):
        if self.pivot:
            self.points[['e', 'n', 'height']]\
                = self.points[['e', 'n', 'height']]\
                + [self.pivotE, self.pivotN, self.pivotH]
            [self.pivotE, self.pivotN, self.pivotH] = [0.0, 0.0, 0.0]
            self.pivot = False
        else:
            pass
